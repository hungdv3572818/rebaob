# ReBaOb - Professional README

## Introduction
ReBaOb is a powerful image processing API that offers a suite of features to enhance and modify images for various applications. Currently, it provides functionalities such as removing background from images and objects, and enhancing image quality. This README provides detailed information on the available APIs and their deployment process.

## Features
1. **Remove Background API**: Removes the background from images, allowing for a seamless and professional appearance.
2. **Remove Object API**: In progress - This feature is currently under development to remove specific objects from images.
3. **Enhance Image**: Not started - This feature will enhance the overall quality and clarity of images.


## Deployment
To deploy ReBaOb, use the following commands:

```bash
docker-compose build
docker-compose up -d
  ```

## Usage

To use ReBaOb APIs, make requests to the respective endpoints based on the features you want to utilize.

### Remove Background API

#### Endpoint
- **Route**: `/rmbg`
- **Method**: `POST`
- **Request Payload**: Tải lên một tệp ảnh.
- **Response**: 
  - **Thành công**: 
    - `success: true`
    - Đối tượng `data` chứa:
      - `svg`: Dữ liệu SVG (nếu có, nếu không sẽ là `null`)
      - `people`: Dữ liệu về người (nếu có, nếu không sẽ là `null`)
      - `mask_array`: Dữ liệu mảng mask dưới định dạng JSON (nếu có, nếu không sẽ là `null`)
  - **Thất bại**:
    - `success: false`
    - `message`: Thông báo lỗi (nếu có)
#### Ví dụ sử dụng
```bash
curl -X POST -F "file=@/đường/dẫn/đến/tệp/ảnh.jpg" http://địa-chỉ-base-của-api/rmbg
```

#### Endpoint Xem Trực Tiếp
- **Route**: `/rmbg_show`
- **Method**: `POST`
- **Request Payload**: Tải lên một tệp ảnh.
- **Response**: 
  - Stream ảnh đã xử lý trực tiếp (nếu thành công).

#### Ví dụ sử dụng
```bash
curl -X POST -F "file=@/đường/dẫn/đến/tệp/ảnh.jpg" http://địa-chỉ-base-của-api/rmbg_show
```

### Remove Object API
- **Endpoint**: `/api/remove-object`
- **Method**: `POST`
- **Request Payload**: ***In progress***
- **Response**: ***In progress***

### Enhance Image
- **Endpoint**: `/api/enhance-image`
- **Method**: `POST`
- **Request Payload**: ***Not started***
- **Response**: ***Not started***
