import cv2
from enum import Enum
import numpy as np
from PIL import Image, ImageOps
from PIL.Image import Image as PILImage


class ReturnType(Enum):
    BYTES = 0
    PILLOW = 1
    NDARRAY = 2

def fix_image_orientation(img: PILImage) -> PILImage:
    """
    Fix the orientation of the image based on its EXIF data.

    Args:
        img (PILImage): The image to be fixed.

    Returns:
        PILImage: The fixed image.
    """
    return ImageOps.exif_transpose(img)
def post_process(mask: np.ndarray) -> np.ndarray:
    """
    Post Process the mask for a smooth boundary by applying Morphological Operations
    Research based on paper: https://www.sciencedirect.com/science/article/pii/S2352914821000757
    args:
        mask: Binary Numpy Mask
    """
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    mask = cv2.GaussianBlur(mask, (5, 5), sigmaX=2, sigmaY=2, borderType=cv2.BORDER_DEFAULT)
    _, mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)

    return mask