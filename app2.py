from fastapi import FastAPI, File, UploadFile, Depends, HTTPException
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.responses import StreamingResponse
from images_process import fix_image_orientation, post_process, ReturnType
import io
import json
import cv2
from typing import Dict, List, Tuple
from PIL import Image
from PIL.Image import Image as PILImage
from json import JSONEncoder
import numpy as np
from detect_face import FaceDetector
from transparent_background import Remover
import time


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

def get_contours_as_json(imgray):
    height, width = imgray.shape
    ret, thresh = cv2.threshold(imgray, 100, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    path_svg = f'<svg width="{width}" height="{height}" xmlns="http://www.w3.org/2000/svg">'
    for contour in contours:
        path_svg += f'<path d="M'
        for i in range(len(contour)):
            x, y = contour[i][0]
            path_svg += f'{x} {y} '
        path_svg += '"/>'

    path_svg += '</svg>'
    with open('output/path.svg', 'w+') as f:
        f.write('<svg width="' + str(width) + '" height="' + str(height) + '" xmlns="http://www.w3.org/2000/svg">')

        for contour in contours:
            f.write('<path d="M')
            for i in range(len(contour)):
                x, y = contour[i][0]
                f.write(str(x) + ' ' + str(y) + ' ')
            f.write('"/>')

        f.write('</svg>')

    json_path_svg = {
        "width": width,
        "height": height,
        "paths": path_svg
    }
    return json_path_svg

app = FastAPI(
    title="Remove Background API",
    description="Remove the background from an uploaded image",
    version="1.0.0",
)

@app.get("/", include_in_schema=False,  tags=['docs'])
async def redirect():
    return RedirectResponse("/docs")

remover = Remover(mode='base-nightly', device='cuda')

async def remove_background(img: Image.Image):
    global remover
    start_time = time.time()

    mask = remover.process(img, type="map")
    mask = np.mean(mask, axis=-1)
    end_time = time.time()

    execution_time = end_time - start_time
    print(f"Execution time: {execution_time} seconds")



    mask_array = (np.array(mask) / 255).astype("float32")
    print(img.size, mask_array.shape)
    new_img = img * mask_array[:, :, np.newaxis]
    new_img = new_img.astype('uint8')
    new_pil_img = Image.fromarray(new_img)
    new_pil_img.save('output/rembg_output.png')

    return mask, new_pil_img


async def remove_background_mask(img: Image.Image):
    global remover
    start_time = time.time()

    mask = remover.process(img, type="map")
    # mask = np.mean(mask, axis=-1)
    end_time = time.time()

    execution_time = end_time - start_time
    print(f"Model Execution time: {execution_time} seconds")

    print(img.size)
    return mask

async def process_data(file: UploadFile = File(...)):
    content = await file.read()
    data = Image.open(io.BytesIO(content))

    print("process_data", type(data))


    try:
        if isinstance(data, Image.Image):
            print("isinstance 1", data.mode)
            if data.mode == 'RGBA':
                print("Your logic for RGBA images")
                data = data.convert('RGB')
            return_type = ReturnType.PILLOW


        elif isinstance(data, bytes):
            print("isinstance 2")
            return_type = ReturnType.BYTES
            data = Image.open(io.BytesIO(data))
        elif isinstance(data, np.ndarray):
            print("isinstance 3")
            return_type = ReturnType.NDARRAY
            if data.shape[2] == 4:
                data = data[:, :, :3]

            data = Image.fromarray(data)
        else:
            raise ValueError("Input type {} is not supported.".format(type(data)))

        return data, return_type
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Failed to process image: {str(e)}")

faceDetector = FaceDetector(label_path="models/voc-model-labels.txt", onnx_path="models/RFB-320-masked_face-v2.onnx")


@app.post("/detect_face")
async def detect_face(file: UploadFile = File(None)):
    if file is not None:
        try:
            result = {
                "success": True,
                "data": {
                    "people": None,
                }
            }
            img, return_type = await process_data(file)
            img = fix_image_orientation(img)
            boxes_face, labels_face, probs_face = faceDetector.predict(img)
            People = len(boxes_face) > 0
            result['data']['people'] = People
            return JSONResponse(content=result)



        except Exception as e:
            result = {
                "success": False,
                "message": f"error {str(e)}"
            }
            return JSONResponse(content=result)
    else:
        result = {
            "success": False,
            "message": "Invalid image file."
        }
        return JSONResponse(content=result)

@app.post("/rmbg_mask", response_class=StreamingResponse)
async def remove_background_route_mask(file: UploadFile = File(None)):
    if file is not None:
        try:
            img, return_type = await process_data(file)
            img = fix_image_orientation(img)
            mask_array = await remove_background_mask(img)
            start = time.time()

            img_bytes = io.BytesIO()
            # mask_array_pil = Image.fromarray(mask_array)
            # mask_array_pil = mask_array_pil.convert('L')
            mask_array.save(img_bytes, format='PNG')
            img_bytes.seek(0)

            end = time.time()
            print(f"Post Processing: {end-start} seconds")

            return StreamingResponse(
                content=img_bytes,
                media_type="image/png",
                headers={"Content-Disposition": "inline; filename=removedbg.png"},
            )
        except Exception as e:

            result = {
                "success": False,
                "message": f"error {str(e)}"
            }
            return JSONResponse(content=result)
    else:
        result = {
            "success": False,
            "message": "Invalid image file."
        }
        return JSONResponse(content=result)




@app.post("/rmbg")
async def remove_background_route(file: UploadFile = File(None)):
    if file is not None:
        try:
            result = {
                "success": True,
                "data": {
                    "svg": None,
                    "people": None,
                    "mask_array": None
                }
            }
            img, return_type = await process_data(file)
            img = fix_image_orientation(img)
            mask_array, new_pil_img = await remove_background(img)
            result['data']['mask_array'] = json.dumps(np.array(mask_array), cls=NumpyArrayEncoder)
            return JSONResponse(content=result)
        except Exception as e:
            result = {
                "success": False,
                "message": f"error {str(e)}"
            }
            return JSONResponse(content=result)
    else:
        result = {
            "success": False,
            "message": "Invalid image file."
        }
        return JSONResponse(content=result)


@app.post("/rmbg_show")
async def remove_background_route_show(file: UploadFile = File(None)):
    if file is not None:
        try:
            result = {
                "success": True,
                "data": {
                    "svg": None,
                    "people": None,
                    "mask_array": None
                }
            }
            img, return_type = await process_data(file)
            img = fix_image_orientation(img)
            mask_array, new_pil_img = await remove_background(img)

            img_bytes = io.BytesIO()
            new_pil_img.save(img_bytes, format='PNG')
            img_bytes.seek(0)

            return StreamingResponse(content=img_bytes, media_type="image/png",
                                     headers={"Content-Disposition": "inline; filename=removedbg.png"})
        except Exception as e:
            result = {
                "success": False,
                "message": f"error {str(e)}"
            }
            return JSONResponse(content=result)
    else:
        result = {
            "success": False,
            "message": "Invalid image file."
        }
        return JSONResponse(content=result)

