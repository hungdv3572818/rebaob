FROM nvidia/cuda:12.1.1-cudnn8-devel-ubuntu22.04

ARG DEBIAN_FRONTEND=noninteractive

ENV PYTHONUNBUFFERED=1

RUN apt-get update && apt-get install --no-install-recommends -y \
  build-essential \
  python3.9 \
  python3-pip \
  git \
  ffmpeg \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /app

ADD requirements.txt /app/requirements.txt

RUN apt-get update && apt-get install -y libgl1-mesa-glx
RUN pip install --upgrade pip
RUN pip install -r requirements.txt && pip install uvicorn

COPY ./ /app

CMD ["bash", "runserver2.sh"]
